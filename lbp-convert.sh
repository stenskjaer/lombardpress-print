#!/bin/bash

#get directory running script
#script copied from http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $SOURCE == /* ]]; then
    echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
echo "SOURCE is '$SOURCE'"
RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
if [ "$DIR" != "$RDIR" ]; then
  echo "DIR '$RDIR' resolves to '$DIR'"
fi
echo "DIR is '$DIR'"

##check for options

#check for options
PDF=true
TEX=true


while getopts ":p:t:" opt; do
  shift $((OPTIND-2)) 
  case $opt in
    p)
    PDF=false
      echo "-p was triggered! PDF option set to false!" >&2
       
      ;;
    t)
    TEX=false
      echo "-t was triggered! TEX option set to false!" >&2
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done



### end of check for options

#config

#custom base directory to the directory that you want your output files to store in
base="/Users/JCWitt/Desktop/lbp_latex_conversion";

echo "Getting xml source version number"
sourceversion="$( git describe --tags --always )"
echo $sourceversion

#get current tag and has of current commit
#if this script every advances to offering multiple style sheets
#the name of the stylesheet used would need to be included as part of the verison number.
echo "Getting style sheet version number"
conversionversion="$( cd -P "$( dirname "$SOURCE" )" && git describe --tags --always)"
echo $conversionversion

startingdirectory=$(pwd)
filename=$1;
subdirectory=$2;

if [ "$TEX" = true ] ; then
  # begin xslt conversion
  echo "Begin TEI to LaTeX conversion";
  saxon "-s:$filename.xml" "-xsl:$DIR/lbp-latex-critical.xsl" "-o:$base/$2/$filename/$filename.tex" "conversionversion=$conversionversion" "sourceversion=$sourceversion";
fi

# move to latex output directory
echo "moving to latex directory"
cd "$base/$2/$filename"	

if [ "$TEX" = true ] ; then
  #stream edit for unwanted spaces
  echo "Begin removing unwanted spaces"
  sed -i.bak -e 's/ \{1,\}/ /g' -e 's/{ /{/g' -e 's/ }/}/g' -e 's/ :/:/g' $filename.tex
  echo "unwanted spaces removed"
fi

if [ "$PDF" = true ] ; then
  echo 'PDF is set to true'
  #begin latex conversion -- assumes pdflatex is command in your path
  echo "Begin LaTeX to PDF conversion";
  pdflatex "$filename.tex";
fi
cd $startingdirectory
echo "I think everything worked";