<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:param name="apploc"><xsl:value-of select="/TEI/teiHeader/encodingDesc/variantEncoding/@location"/></xsl:param>
    <xsl:param name="notesloc"><xsl:value-of select="/TEI/teiHeader/encodingDesc/variantEncoding/@location"/></xsl:param>
    <xsl:variable name="title"><xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/title"/></xsl:variable>
    <xsl:variable name="author"><xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/author"/></xsl:variable>
    <xsl:variable name="editor"><xsl:value-of select="/TEI/teiHeader/fileDesc/titleStmt/editor"/></xsl:variable>
    <xsl:param name="targetdirectory">null</xsl:param>
    <!-- get versioning numbers -->
    <xsl:param name="sourceversion"><xsl:value-of select="/TEI/teiHeader/fileDesc/editionStmt/edition/@n"/></xsl:param>

    <!-- this xsltconvnumber should be the same as the git tag, and for any commit past the tag should be the tag name plus '-dev' -->
    <xsl:param name="conversionversion">dev</xsl:param>

    <!-- default is dev; if a unique version number for the print output is desired; it should be passed as a parameter -->

    <!-- combined version number should have mirror syntax of an equation x+y source+conversion -->
    <xsl:variable name="combinedversionnumber"><xsl:value-of select="$sourceversion"/>+<xsl:value-of select="$conversionversion"/></xsl:variable>
    <!-- end versioning numbers -->
    <xsl:variable name="fs"><xsl:value-of select="/TEI/text/body/div/@xml:id"/></xsl:variable>
    <xsl:variable name="name-list-file">/Users/michael/Documents/PhD/transcriptions/tools/lists/prosopography.xml</xsl:variable>
    <xsl:variable name="work-list-file">/Users/michael/Documents/PhD/transcriptions/tools/lists/workscited.xml</xsl:variable>

    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="*/text()[normalize-space()]">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="replace(., '\s+', ' ')"/>
    </xsl:template>

    <xsl:template match="/">
        %this tex file was auto produced from TEI by lombardpress-print on <xsl:value-of  select="current-dateTime()"/> using the  <xsl:value-of select="base-uri(document(''))"/> 
        \documentclass[twoside, openright, a4paper]{scrbook}

        %imakeidx must be loaded beore eledmac
        \usepackage{imakeidx}
        \usepackage{titlesec}
        \usepackage{libertine}

        \usepackage [autostyle, english = american]{csquotes}
        \usepackage{reledmac}

        \usepackage{geometry}
        \geometry{left=4cm, right=4cm, top=3cm, bottom=3cm}

        \usepackage{fancyhdr}
        %fancyheading settings
        \pagestyle{fancy}

        %git package
        \usepackage{gitinfo2}


        %title settings
        \titleformat{\section} {\normalfont\scshape}{\thesection}{1em}{}
        \titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{12pt plus 2pt minus 2pt}
        \titleformat{\chapter} {\normalfont\Large\uppercase}{\thechapter}{50pt}{}

        %reledmac settings
        \Xinplaceoflemmaseparator{0pt}     % Don't add space after nolemma notes
        <!-- \Xarrangement{paragraph}           % Arrange all apparatuses in paragraphs -->
        \linenummargin{outer}
        \sidenotemargin{inner}

        %other settings
        \linespread{1.1}

        %custom macros
        \newcommand{\name}[1]{\textsc{#1}}
        \newcommand{\worktitle}[1]{\textit{#1}}
        \newcommand{\supplied}[1]{\{#1\}}
        \newcommand{\secluded}[1]{\[#1\]}
        \newcommand{\hand}[1]{\textsuperscript{#1}}

        \begin{document}
        \fancyhead{}
        \fancyfoot{}
        \fancyhead[RO]{<xsl:value-of select="$title"/>}
        \fancyhead[LO]{<xsl:value-of select="$author"/>}
        <xsl:if test="/TEI/teiHeader/revisionDesc/@status = 'draft'">
            \fancyhead[C]{DRAFT}
        </xsl:if>

        \chapter*{<xsl:value-of select="$title"/>}
        \addcontentsline{toc}{chapter}{<xsl:value-of select="$title"/>}

        <xsl:apply-templates select="//body"/>
        \end{document}
    </xsl:template>

    <xsl:template match="div//head">\section*{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="div//div">
        \bigskip
        <xsl:apply-templates/>

    </xsl:template>
    <xsl:template match="p">
        <xsl:variable name="pn"><xsl:number level="any" from="tei:text"/></xsl:variable>
        \pstart
        \ledsidenote{\textbf{<xsl:value-of select="$pn"/>}}
        <xsl:apply-templates/>
        \pend
    </xsl:template>
    <xsl:template match="head">
    </xsl:template>
    <xsl:template match="div">
        \beginnumbering
        <xsl:apply-templates/>
        \endnumbering
    </xsl:template>

    <xsl:template match="unclear">\emph{<xsl:apply-templates/> [?]}</xsl:template>
    <xsl:template match="app//unclear"><xsl:apply-templates/> ut vid.</xsl:template>
    <xsl:template match="q | term">\emph{<xsl:apply-templates/>}</xsl:template> <!-- Does not work in app! -->
    <xsl:template match="pb | cb"><xsl:variable name="MsI"><xsl:value-of select="translate(./@ed, '#', '')"/></xsl:variable>\ledsidenote{<xsl:value-of select="concat($MsI, ./@n)"/>}</xsl:template>
    <xsl:template match="supplied">\supplied{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="secl">\secluded{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="note">\footnoteA{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="del">\st{<xsl:apply-templates/>}</xsl:template>
    <xsl:template match="add">[+ <xsl:apply-templates/>, ms.]</xsl:template>
    <xsl:template match="tei:lb">
        <xsl:choose>
            <xsl:when test="@break='no'"/>
            <xsl:otherwise>
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="cit[bibl]">
        <xsl:text>\edtext{\enquote{</xsl:text>
        <xsl:apply-templates select="quote"/>
        <xsl:text>}}{</xsl:text>
        <xsl:if test="count(tokenize(normalize-space(./quote), ' ')) &gt; 10">
            <xsl:text>\lemma{</xsl:text>
            <xsl:value-of select="tokenize(normalize-space(./quote), ' ')[1]"/>
            <xsl:text> \dots\ </xsl:text>
            <xsl:value-of select="tokenize(normalize-space(./quote), ' ')[last()]"/>
            <xsl:text>}</xsl:text>
        </xsl:if>
        <xsl:text>\Afootnote{</xsl:text>
        <xsl:apply-templates select="bibl"/>
        <xsl:text>}}</xsl:text>
    </xsl:template>
    <xsl:template match="ref[bibl]">
        <xsl:text>\edtext{</xsl:text>
        <xsl:apply-templates select="seg"/>
        <xsl:text>}{</xsl:text>
        <xsl:if test="count(tokenize(normalize-space(./seg), ' ')) &gt; 10">
            <xsl:text>\lemma{</xsl:text>
            <xsl:value-of select="tokenize(normalize-space(./seg), ' ')[1]"/>
            <xsl:text> \dots\ </xsl:text>
            <xsl:value-of select="tokenize(normalize-space(./seg), ' ')[last()]"/>
            <xsl:text>}</xsl:text>
        </xsl:if>
        <xsl:text>\Afootnote{</xsl:text>
        <xsl:apply-templates select="bibl"/>
        <xsl:text>}}</xsl:text>
    </xsl:template>
    <xsl:template match="ref"><xsl:apply-templates/></xsl:template>

    <!-- The apparatus template -->
    <xsl:template match="app">
        <xsl:variable name="tok-before" select="tokenize(normalize-space(string-join(preceding::text(),'')),' ')" />
        <xsl:variable name="lemmaContent">
            <xsl:choose>
                <xsl:when test="./lem and not(./lem = '')">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:text>\edtext{\textbf{</xsl:text>
        <xsl:apply-templates select="lem"/>
        <xsl:text>}}{</xsl:text>
        <xsl:choose>
            <xsl:when test="count(tokenize(normalize-space(./lem), ' ')) &gt; 10">
                <xsl:text>\lemma{</xsl:text>
                <xsl:value-of select="tokenize(normalize-space(./lem), ' ')[1]"/>
                <xsl:text> \dots\ </xsl:text>
                <xsl:value-of select="tokenize(normalize-space(./lem), ' ')[last()]"/>
                <xsl:text>}</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\lemma{</xsl:text>
                <xsl:apply-templates select="lem"/>
                <xsl:text>}</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="$lemmaContent = 1">
                <xsl:text>\Bfootnote{</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>\Bfootnote[nosep]{</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:for-each select="./rdg">
            <xsl:call-template name="varianttype">
                <xsl:with-param name="precedingWord" select="subsequence($tok-before,count($tok-before))" />
            </xsl:call-template>
        </xsl:for-each>
        <xsl:if test="note">
            <xsl:text> (</xsl:text><xsl:value-of select="note"/><xsl:text>)</xsl:text>
        </xsl:if>
        <xsl:text>}}</xsl:text>
    </xsl:template>


    <xsl:template match="name">
        <xsl:variable name="nameid" select="substring-after(./@ref, '#')"/>
        <xsl:text> \name{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text><xsl:text>\index[persons]{</xsl:text><xsl:value-of select="document($name-list-file)//tei:person[@xml:id=$nameid]/tei:persName[1]"/><xsl:text>}</xsl:text>
    </xsl:template>
    <xsl:template match="title">
        <xsl:variable name="workid" select="substring-after(./@ref, '#')"/>
        <xsl:text>\worktitle{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text><xsl:text>\index[works]{</xsl:text><xsl:value-of select="document($work-list-file)//tei:bibl[@xml:id=$workid]/tei:title[1]"/><xsl:text>}</xsl:text>
    </xsl:template>
    <xsl:template match="mentioned">
        <xsl:text>\enquote*{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}</xsl:text>
    </xsl:template>
    
    <xsl:template match="quote"><xsl:apply-templates/></xsl:template>
    <xsl:template match="rdg"></xsl:template>
    <xsl:template match="app/note"></xsl:template>


    <xsl:template name="varianttype">
        <xsl:param name="precedingWord" />
        <xsl:choose>
            <xsl:when test="./note">
                <xsl:text>\emph{</xsl:text>
                <xsl:value-of select="./note"/>
                <xsl:text>} </xsl:text>
                <xsl:value-of select="translate(@wit, '#', '')"/><xsl:text> </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="./del">
                        <xsl:value-of select="./del"/>
                        <xsl:text> post </xsl:text>
                        <xsl:value-of select="$precedingWord"/>
                        <xsl:text> del. </xsl:text>
                        <xsl:call-template name="getWitSiglum"/>
                    </xsl:when>
                    <xsl:when test="./add">
                        <xsl:value-of select="./add"/>
                        <xsl:call-template name="getLocation" />
                        <xsl:text> add. </xsl:text>
                        <xsl:call-template name="getWitSiglum"/>
                    </xsl:when>
                    <xsl:when test="./subst">
                        <xsl:value-of select="./subst/add"/>
                        <xsl:text> corr. ex </xsl:text>
                        <xsl:value-of select="./subst/del"/>
                        <xsl:text> </xsl:text>
                        <xsl:call-template name="getWitSiglum"/>
                    </xsl:when>
                    <xsl:when test="./unclear/@reason = 'rasura'">
                        <xsl:text>post </xsl:text>
                        <xsl:value-of select="$precedingWord"/>
                        <xsl:text> ras. </xsl:text>
                        <xsl:value-of select="./unclear/@extent" />
                        <xsl:text> litteras </xsl:text>
                        <xsl:call-template name="getWitSiglum"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="."/><xsl:text> </xsl:text>
                        <xsl:call-template name="getWitSiglum"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="getLocation">
        <xsl:choose>
            <xsl:when test="./add/@place='above'">
                <xsl:text> \textit{sup. lin.}</xsl:text>
            </xsl:when>
            <xsl:when test="contains(./add/@place, 'margin')">
                <xsl:text> \textit{in marg.}</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="getWitSiglum">
        <xsl:variable name="appnumber"><xsl:number level="any" from="tei:text"/></xsl:variable>
        <xsl:value-of select="translate(./@wit, '#', '')"/>
        <xsl:if test=".//@hand">
            <xsl:text>\hand{</xsl:text>
            <xsl:for-each select=".//@hand">
                <xsl:value-of select="translate(., '#', '')"/>
                <xsl:if test="not(position() = last())">, </xsl:if>
            </xsl:for-each>
            <xsl:text>}</xsl:text>
        </xsl:if>

        <xsl:text> n</xsl:text><xsl:value-of select="$appnumber"></xsl:value-of>
    </xsl:template>

</xsl:stylesheet>
